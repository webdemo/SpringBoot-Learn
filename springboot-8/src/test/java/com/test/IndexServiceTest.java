package com.test;

import com.start.AppApplication;
import com.start.service.IndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ClassName: IndexControllerTest
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/9
 */
@RunWith(SpringJUnit4ClassRunner.class)
//指定SpringBoot工程的Application启动类
//支持web项目
@WebAppConfiguration
@SpringBootTest(classes = AppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class IndexServiceTest {
    @Autowired
    private IndexService indexService;

    @Test
    public void indexServiceTest() {
        String body = indexService.getStr();
        assertThat(body).isEqualTo("ok");
    }
}
