#kibana登录认证
##1.介绍
beats是一个代理，将不同类型的数据发送到elasticsearch。beats可以直接将数据发送到elasticsearch，也可以通过logstash将数据发送elasticsearch。
beats有三个典型的例子：Filebeat、Topbeat、Packetbeat。Filebeat用来收集日志，Topbeat用来收集系统基础设置数据如cpu、内存、每个进程的统计信息，Packetbeat是一个网络包分析工具，统计收集网络信息。这三个是官方提供的。后续会慢慢介绍这三个beat。
ELK的目标是建立一个平台，可以很容易的创建新的beats。为了这个目录，开发了libbeat，该Go库包含了所有beats公共部分来处理任务，如批量插入到elasticsearch，安全的发送事件到logstash，logstash和elasticsearch多节点的负载均衡，异步或同步的发送事件模式。该libbeat平台还提供了检测机制，当下游服务器负载高或者网络拥堵，自动的降低发送速率。
##x-pack
```shell

    ./bin/kibana-plugin install x-pack
    ./bin/elasticsearch-plugin install x-pack
    
```
##
configure action.auto_create_index to elasticsearch.yml
```shell
    
    action.auto_create_index: .security,.monitoring*,.watches,.triggered_watches,.watcher-history*

```

##默认账号密码

```shell
curl -XGET -u elastic:1qaz@WSX3edc  '10.25.228.201:9200/_cat/indices?v'
curl -XDELETE -u elastic:1qaz@WSX3edc  "10.25.228.201:9200/logstash-system-2016.11.13"
curl -XGET -u elastic:1qaz@WSX3edc  "10.25.228.201:9200/_cluster/health?pretty"



curl  -u elastic:1qaz@WSX3edc -XPUT 10.25.228.201:9200/_cluster/settings -d '{
    "persistent" : {
        "discovery.zen.minimum_master_nodes" : 2
    }
}'
```