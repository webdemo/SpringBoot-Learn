package com.spring.boot.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: AppApplication
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/7
 */
@SpringBootApplication
public class AppApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(AppApplication.class, args);
    }
}
