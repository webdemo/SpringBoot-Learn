package com.test;

import com.start.AppApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ClassName: IndexControllerTest
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/19
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IndexControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;
    //注入端口号
    @LocalServerPort
    private int port;
    @Test
    public void indexControllerTest() {
        String body = this.restTemplate.getForObject("/index", String.class);
        assertThat(body).isEqualTo("ok"+port);
    }
}
