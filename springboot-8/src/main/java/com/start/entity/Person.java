package com.start.entity;

import java.io.Serializable;

/**
 * ClassName: Person
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/19
 */
public class Person implements Serializable {
    private int age;
    private String name;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
